import { RouterModule, Routes } from "@angular/router";
import { ContactosComponent } from "./components/contactos/contactos.component";
import { InicioComponent } from "./components/inicio/inicio.component";
import { PortafolioComponent } from "./components/portafolio/portafolio.component";
import { SobreMiComponent } from "./components/sobre-mi/sobre-mi.component";


const APP_ROUTES: Routes = [
  {path: 'Inicio', component : InicioComponent },
  {path: 'sobre-mi', component: SobreMiComponent},
  {path: 'portafolio', component: PortafolioComponent},
  {path: 'contactos', component: ContactosComponent},
  
  
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES)