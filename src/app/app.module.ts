import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { SobreMiComponent } from './components/sobre-mi/sobre-mi.component';
import { PortafolioComponent } from './components/portafolio/portafolio.component';
import { ContactosComponent } from './components/contactos/contactos.component';
import { APP_ROUTING } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    SobreMiComponent,
    PortafolioComponent,
    ContactosComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING
    
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
